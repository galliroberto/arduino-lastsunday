#include "Arduino.h"
#include <string.h>
#include "LastSunday.h"

 
int year;
String m[12];
bool isleap;

void LastSunday::lastSunday()
{
	m[0]  = "JANUARY:   "; m[1]  = "FEBRUARY:  "; m[2]  = "MARCH:     "; m[3]  = "APRIL:     "; 
	m[4]  = "MAY:       "; m[5]  = "JUNE:      "; m[6]  = "JULY:      "; m[7]  = "AUGUST:    "; 
	m[8]  = "SEPTEMBER: "; m[9]  = "OCTOBER:   "; m[10] = "NOVEMBER:  "; m[11] = "DECEMBER:  "; 
}

void LastSunday::isleapyear()
{
	isleap = false;
	if( !( year % 4 ) )
	{
		if( year % 100 ) isleap = true;
		else if( !( year % 400 ) ) isleap = true;
	}
}

int LastSunday::getWeekDay(int m, int d)
{
	int y = year;
 
	int f = y + d + 3 * m - 1;
	m++;
	if( m < 3 ) y--;
		else f -= int( .4 * m + 2.3 );
 
	f += int( y / 4 ) - int( ( y / 100 + 1 ) * 0.75 );
	f %= 7;
 
	return f;
}

void LastSunday::byYear(int y, int *lastSundayOfAllMonth)
{
	year = y;
	this->isleapyear();
 
	int days[] = { 31, isleap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, d;
	for( int i = 0; i < 12; i++ )
	{
		d = days[i];
		while( true )
		{
		if( !this->getWeekDay( i, d ) ) break;
			d--;
		}
		lastSundayOfAllMonth[i] = d;
	}
}

int LastSunday::byYearMonth(int y, int month)
{
	year = y;
	this->isleapyear();
 
	int days[] = { 31, isleap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }, d;
	month--;
	d = days[month];
	while( true )
	{
	if( !this->getWeekDay( month, d ) ) break;
		d--;
	}

	return d;
}

		
	 

