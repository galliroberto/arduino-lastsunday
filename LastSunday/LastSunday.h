/*
	LastSunday.h - Library for get the last sunday of Month by Year / Month.
	Created by Robbygallo, based on http://rosettacode.org/wiki/Find_the_last_Sunday_of_each_month.
	Released into the public domain.

	How to use:
	
	#include <LastSunday.h>
	LastSunday ls;
	
	// get the last sunday day number of the month
	dayNumber = ls.byYearMonth(yearNumber, monthNumber); 
	
	// get the last sunday day number of all year's months
	int lastSundayMonths[12];
	ls.byYear(yearNumber, lastSundayMonths);
	
	// Print all last sunday for each month
	for(int i=0; i<12; i++) {
		Serial.println(String(i+1) + ": " + String(lastSundayMonths[i]));
	}
*/
#ifndef LastSunday_h
#define LastSunday_h

#include "Arduino.h"

class LastSunday
{
	public:
		void byYear(int y, int *lastSundayOfAllMonth);
		int byYearMonth(int y, int month);
		void lastSunday();
	private:
		void isleapyear();
		int getWeekDay(int m, int d);
};

#endif