Arduino Library for get the last sunday of Month by Year / Month.

Based on http://rosettacode.org/wiki/Find_the_last_Sunday_of_each_month.

 

How to use:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <LastSunday.h>
LastSunday ls;

// get the last sunday day number of the month
dayNumber = ls.byYearMonth(yearNumber, monthNumber); 

// get the last sunday day number of all year's months
int lastSundayMonths[12];
ls.byYear(yearNumber, lastSundayMonths);

// Print all last sunday for each month
for(int i=0; i\<12; i++) {
    Serial.println(String(i+1) + ": " + String(lastSundayMonths[i]));
}   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

example to use with GMT+1

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// year = year of my date
// month = month of my date
// day = year of my date

int lastSundayNumber = ls.byYearMonth(year, month);

// GMT+1
int adjustHour = 1;

// for DST zone, this example is based on Italy :: from last sunday of march to last sunday of october
if(month == 3 && day >= lastSundayNumber || month >= 4 && month <= 9 || month == 10 && day <= lastSundayNumber) {
    adjustHour = 2;
}

String timeText = String(hour+adjustHour)+":"+String(minute);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
